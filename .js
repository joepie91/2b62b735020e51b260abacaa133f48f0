function makeRequest(url) {
  return Promise.try(() => {
    return bhttp.get(url); // Whether an error occurs here...
  }).then((response) => {
    return response.body;  // ... or here...
  });
}

function makeManyRequests(urls) {
  return Promise.try(() => {
    if (urls.length === 0) {
      throw new Error("Must specify at least one URL"); // ... or here...
    } else {
      return urls;
    }
  }).map((url) => {
    return makeRequest(url);
  });
}

function doMyThing() {
  return Promise.try(() => {
    return makeManyRequests([
      "http://google.com/",
      "http://bing.com/",
      "http://yahoo.com"
    ]);
  }).catch((err) => {
    console.log("Oh noes!", err); // ... it will always end up here.
  });
}